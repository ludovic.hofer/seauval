# SEAUVAL: SEmi-AUtomatic eVALution

A set of python tools based on package `anytree` to assist teachers in producing
valuable and explicit feedback for students.

## Building

`make build`

## Local install

`pip3 install --force-reinstall dist/seauval-X.X-py3-none-any-whl`

## Upload to pip repository
`python -m twine upload dist/*`
