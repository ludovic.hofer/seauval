build:
	python3 setup.py bdist_wheel

upload:
	python3 -m twine upload dist/*

mrproper:
	rm -rf build dist seauval.egg-info

.PHONY: build upload mrproper
